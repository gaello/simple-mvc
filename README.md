# Simple MVC for Unity

So you are looking for information how to implement MVC in Unity? There is a lot of complex implementations of that design pattern which require a lot of programming knowledge. So this is simple and easy to understand implementation of that design pattern! Don't believe me? See it on your own ;)

In addition to this repository I also made a post about it that you can find here: https://www.patrykgalach.com/2019/04/29/simple-mvc-for-unity/

Enjoy!

---

# How to use it?

This repository contains an example of how you can implement MVC design pattern in Unity.

If you want to see that implementation, go straight to [Assets/Scripts/](https://bitbucket.org/gaello/simple-mvc/src/master/Assets/Scripts/) folder. You will find all code that I wrote to make it work. Code also have comments so it would make a little bit more sense.

I hope you will enjoy it!

---

#Well done!

You have just learned how to implement simple MVC in Unity!

##Congratulations!

For more visit my blog: https://www.patrykgalach.com
